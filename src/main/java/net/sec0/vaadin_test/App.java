package net.sec0.vaadin_test;

import com.vaadin.annotations.Theme;
import com.vaadin.cdi.CDIUI;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author markus-lauer
 */
@CDIUI("")
@Theme("valo")
@ApplicationScoped
public class App extends UI {

    @Override
    protected void init(VaadinRequest request) {

        getPage().setTitle("vaadin-test");
        
        VerticalLayout layout = new VerticalLayout();
        layout.addComponent(new Label("hallo"));

        Button btn = new Button("drück mich");
        btn.addClickListener((event) -> layout.addComponent(new Label("gut so")));

        layout.addComponent(btn);

        setContent(layout);
    }

}
