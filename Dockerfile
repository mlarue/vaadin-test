FROM jboss/wildfly:10.1.0.Final

ENV VAADIN_MODULE wildfly-vaadin-0.1.0-SNAPSHOT-bin.zip

WORKDIR $JBOSS_HOME

COPY $VAADIN_MODULE $JBOSS_HOME
RUN unzip $VAADIN_MODULE && rm $VAADIN_MODULE

COPY target/vaadin-test.war $JBOSS_HOME/standalone/deployments